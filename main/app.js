function addTokens(input, tokens) {
    if (input.constructor !== String) {
        throw new Error("Invalid input")
    }

    if (input.length < 6) {
        throw new Error("Input should have at least 6 characters")
    }

    for (let key in tokens[0]) {
        if (tokens[0][key].constructor !== String) {
            // console.log(tokens[0][key])
            throw new Error("Invalid array format")
        }
    }
    
    for (let key in tokens[0]) {
        let rep = '${' + tokens[0][key] + '}'
        input = input.replace('...', rep)
    }

    return input
}

const app = {
    addTokens: addTokens
}

module.exports = app